package cat712.souvenirs;

import cat712.souvenirs.action.ModificationAction;
import cat712.souvenirs.action.impl.ModificationActionImpl;
import cat712.souvenirs.entity.Country;
import cat712.souvenirs.entity.Producer;
import cat712.souvenirs.entity.Souvenir;
import cat712.souvenirs.dao.AdditionalDAO;
import cat712.souvenirs.dao.SouvenirDAO;
import cat712.souvenirs.dao.impl.CountryDAOImpl;
import cat712.souvenirs.dao.impl.ProducerDAOImpl;
import cat712.souvenirs.dao.impl.SouvenirDAOImpl;
import cat712.souvenirs.print.PrintingResult;

import java.math.BigDecimal;

/**
 * Класс для запуска приложения
 * @version 1.0
 */
public class App {
    public static void main(String[] args) {
        final SouvenirDAO<Long, Souvenir> souvenirDAO = new SouvenirDAOImpl();
        final AdditionalDAO<Long, Producer> producerDAO = new ProducerDAOImpl();
        final AdditionalDAO<Long, Country> countryDAO = new CountryDAOImpl();
        PrintingResult printingResult = new PrintingResult(souvenirDAO, producerDAO, countryDAO);

        Producer producer = producerDAO.read(2L).get();
        printingResult.souvenirsSpecifiedProducer(producer);
        System.out.println();

        Country country = countryDAO.read(4L).get();
        printingResult.souvenirsProducedInSpecifiedCountry(country);
        System.out.println();

        printingResult.producersWithPricesForSouvenirsLessThanSpecified(new BigDecimal(1200));
        System.out.println();

        printingResult.producersSouvenirsProducedInSpecifiedYear("Чайник", 2018);
        System.out.println();

        ModificationAction modificationAction = new ModificationActionImpl(souvenirDAO, producerDAO);
        boolean resultDelete = modificationAction.deleteProducerAndHisSouvenirs(producer);
        if (resultDelete) {
            System.out.println("Удаление производителя '" + producer.getName() + "' и его сувениров выполнено");
        } else {
            System.out.println("Удаление производителя '" + producer.getName() + "' не выполнено");
        }
        System.out.println();
        printingResult.souvenirsSpecifiedProducer(producer);
    }
}
